Passando só para adiantar alguns detalhes sobre a nossa mini-demo de hoje!
Horário: 15:15h
Local: https://kenzieacademybr.zoom.us/my/mariaporcina
Equipe: @Maria Porcina, @Victor Ivan - Peer Coach 11-13h (Q1), @Ioane Faumui - Peer Coach 13-15hrs Q1 e @Marcone Melo;
Nela abordaremos:
HTML semântico (HTML 5);
Conceitos de Flexbox, Pseudoclasses e Absolute Position em  CSS3:css3:; e
Algumas boas práticas com git :git: .
Para quem queira ter acesso ao material público de hoje, seguem os links:
Mock-up, no Figma: https://www.figma.com/file/jJmIbR8MItr8LoygYAj8lc/SMARTWATCH?node-id=0%3A1 (apenas o "Desktop 3); e
Repositório onde serão commitados todos os códigos de hoje (façam um "fork", caso queiram): https://gitlab.com/vitoivan/mini-demo1
Simboooora!!!
